export const searchByKey = (key, keValue, arr) => {
  for (let i = 0; i < arr.length; i++) {
    if (arr[i][key] === keValue) {
      return arr[i];
    }
  }
  return '';
}